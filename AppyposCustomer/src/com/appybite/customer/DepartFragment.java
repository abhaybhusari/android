package com.appybite.customer;

import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appybite.customer.info.CategoryInfo;
import com.appybite.customer.info.DepartInfo;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.yj.commonlib.image.AnimateFirstDisplayListener;
import com.yj.commonlib.image.Utils;
import com.yj.commonlib.network.NetworkUtils;
import com.yj.commonlib.pref.PrefValue;
import com.yj.commonlib.screen.LayoutLib;
import com.yj.commonlib.screen.PRJFUNC;

public class DepartFragment extends Fragment {

	private DisplayImageOptions options;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	private DepartInfo departInfo;

	private RelativeLayout rlDepartInfo;
	private ImageView ivDepartBg;
	private TextView tvDepartDesc, tvDepartDesc1, tvArrivedate, tvDepartdate;
	private DatePicker datepicker;
	private ImageView dateImageview;
	private ListView lvCategoryList;
	private CategoryListAdapter m_adtCategoryList;
	private ProgressBar pbCategory;
	private com.appybite.customer.HorizontalListView horizontalListView;
	private RelativeLayout rlDepartment;
	private boolean loadFlag = false;

	private int orientation;
	private boolean loadImageFlag = false;
	Calendar c = Calendar.getInstance();
	Dialog custom;
	int date, month, year;

	public DepartFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setRetainInstance(true);
		orientation = getResources().getConfiguration().orientation;
		View v = inflater.inflate(R.layout.frag_depart, container, false);
		lvCategoryList = (ListView) v.findViewById(R.id.lvCategoryList);
		horizontalListView = (com.appybite.customer.HorizontalListView) v
				.findViewById(R.id.hlvCategoryList);
		rlDepartInfo = (RelativeLayout) v.findViewById(R.id.rlDepartInfo);
		rlDepartment = (RelativeLayout) v
				.findViewById(R.id.mainFragmentContainer);

		tvArrivedate = (TextView) v.findViewById(R.id.txt_arrive_date);
		tvDepartdate = (TextView) v.findViewById(R.id.txt_depart_date);
		// datepicker=(DatePicker)v.findViewById(R.id.date);
		dateImageview = (ImageView) v.findViewById(R.id.iv_calender);

		RelativeLayout rl = (RelativeLayout) v.findViewById(R.id.ll_radio);
		if ((departInfo.title.equals("Villas To Rent"))) {
			rl.setVisibility(View.VISIBLE);
		} else {
			rl.setVisibility(View.GONE);
		}

		date = c.get(Calendar.DAY_OF_MONTH);
		month = c.get(Calendar.MONTH);
		year = c.get(Calendar.YEAR);

		String ariveDate = "" + date + "/" + (month + 1) + "/" + year;
		tvArrivedate.setText(ariveDate);
		Button btnbooking = (Button) v.findViewById(R.id.btnbook);
		btnbooking.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				custom = new Dialog(getActivity());
				custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
				custom.setContentView(R.layout.register);
				custom.getWindow().setLayout(600,350);
				
			//	custom.setTitle(Window.FEATURE_NO_TITLE);
				// final RelativeLayout rlBookingDetails =
				// (RelativeLayout)custom.findViewById(R.id.rl_BookingDetails);
				final RelativeLayout bookingD = (RelativeLayout) custom
						.findViewById(R.id.rl_booking);
				final ImageButton imgbook = (ImageButton) custom
						.findViewById(R.id.bookingdetailimge_1);
				final ImageButton imgbookMonus = (ImageButton) custom
						.findViewById(R.id.bookingdetail_minus);

				bookingD.setVisibility(View.GONE);
				imgbookMonus.setVisibility(View.GONE);
				imgbook.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						imgbookMonus.setVisibility(View.VISIBLE);
						bookingD.setVisibility(View.VISIBLE);
						imgbook.setVisibility(View.GONE);
					}
				});

				imgbookMonus.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						imgbookMonus.setVisibility(View.GONE);
						bookingD.setVisibility(View.GONE);
						imgbook.setVisibility(View.VISIBLE);

					}
				});

				final RelativeLayout rlCondition1 = (RelativeLayout) custom
						.findViewById(R.id.rl_condition1);
				final ImageButton btnConditionPlus = (ImageButton) custom
						.findViewById(R.id.btnBookingConditionplus);
				final ImageButton btnConditionMinus = (ImageButton) custom
						.findViewById(R.id.btnBookingConditionminus);

				btnConditionMinus.setVisibility(View.GONE);
				rlCondition1.setVisibility(View.GONE);
				btnConditionPlus.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						btnConditionMinus.setVisibility(View.VISIBLE);
						btnConditionPlus.setVisibility(View.GONE);
						rlCondition1.setVisibility(View.VISIBLE);

					}
				});

				btnConditionMinus.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						btnConditionMinus.setVisibility(View.GONE);
						btnConditionPlus.setVisibility(View.VISIBLE);
						rlCondition1.setVisibility(View.GONE);
					}
				});

				final RelativeLayout rlPrice = (RelativeLayout) custom
						.findViewById(R.id.rl_PriceDetail);
				final ImageButton priceBtnMinus = (ImageButton) custom
						.findViewById(R.id.btnPriceMinus);
				final ImageButton priceBtnPlus = (ImageButton) custom
						.findViewById(R.id.btnPricePlus);

				rlPrice.setVisibility(View.GONE);
				priceBtnMinus.setVisibility(View.GONE);

				priceBtnPlus.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub

						rlPrice.setVisibility(View.VISIBLE);
						priceBtnMinus.setVisibility(View.VISIBLE);
						priceBtnPlus.setVisibility(View.GONE);
						
					}
				});

				priceBtnMinus.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
					
						priceBtnMinus.setVisibility(View.GONE);
						rlPrice.setVisibility(View.GONE);
						priceBtnPlus.setVisibility(View.VISIBLE);

						
					}   
				});
		
				 	final RelativeLayout rl_guestdetail = (RelativeLayout) custom
						.findViewById(R.id.rl_guestDetails1);
				final ImageButton guestBtnMinus = (ImageButton) custom
						.findViewById(R.id.btnGuestMinus);
				final ImageButton guestBtnPlus = (ImageButton) custom
						.findViewById(R.id.btnGuestPlus);

				rl_guestdetail.setVisibility(View.GONE);
				guestBtnMinus.setVisibility(View.GONE);

				guestBtnPlus.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub

						rl_guestdetail.setVisibility(View.VISIBLE);
						guestBtnMinus.setVisibility(View.VISIBLE);
						guestBtnPlus.setVisibility(View.GONE);
						
					}
				});

				guestBtnMinus.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
					
						guestBtnMinus.setVisibility(View.GONE);
						rl_guestdetail.setVisibility(View.GONE);
						guestBtnPlus.setVisibility(View.VISIBLE);

						
					}   
				});
				 
				 
				
				 Button btncontinue=(Button)custom.findViewById(R.id.btn_continue);
				 btncontinue.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) 
					{
						Dialog custom1 = new Dialog(getActivity());
						

						custom1.requestWindowFeature(Window.FEATURE_NO_TITLE);
						custom1.setContentView(R.layout.pay_continue);
						custom1.getWindow().setLayout(600,350);
						custom1.show();
						
					}
				});
				custom.show();
				
			}
		});

		dateImageview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				custom = new Dialog(getActivity());

				custom.setContentView(R.layout.test);

				custom.setTitle("Set Depart Date");
				/* custom.getWindow().setLayout(400,500); */

				Button cancel = (Button) custom.findViewById(R.id.btnCancel);
				cancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						custom.dismiss();
					}
				});

				Button set = (Button) custom.findViewById(R.id.btnSet);
				set.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						DatePicker dt = (DatePicker) custom
								.findViewById(R.id.datePicker1);
						int d = dt.getDayOfMonth();
						int m = dt.getMonth();
						int y = dt.getYear();
						String dat = "" + d + "/" + (m + 1) + "/" + y;
						tvDepartdate.setText(dat);
						custom.dismiss();
					}
				});
				custom.show();

			}
			
		});
		
		
		 

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView(v);
		}

		// rlDepartInfo.setVisibility(View.VISIBLE);
		if (departInfo.id > 0) {
			loadImageFlag = true;
			departInfo.image = departInfo.image.replaceAll(" ", "%20");
			ImageLoader.getInstance().displayImage(departInfo.image,
					ivDepartBg, options, animateFirstListener);
		}
		if (!loadFlag && NetworkUtils.haveInternet(getActivity())
				&& departInfo.id == 0) {
			loadBackgroundImage(PrefValue.getString(getActivity(),
					R.string.pref_hotel_background_image));
			loadFlag = true;
		} else if (departInfo.id > 0
				&& orientation == Configuration.ORIENTATION_LANDSCAPE) {
			departInfo.image = departInfo.image.replaceAll(" ", "%20");
			loadBackgroundImage(departInfo.image);
			// rlDepartment.setBackgroundResource(departInfo.image);
		}

		// ivDepartBg.setImageResource(R.drawable.bg_default_restaurant);

		// tvDepartDesc = (TextView) v.findViewById(R.id.tvDepartDesc);
		tvDepartDesc.setText(departInfo.desc);
		tvDepartDesc1.setText(departInfo.desc);
		loadCategoryList();

		return v;
	}

	private void loadBackgroundImage(final String url) {

		new AsyncTask<String, Void, String>() {
			Bitmap bitmap = null;

			@Override
			protected String doInBackground(String... params) {
				// TODO Auto-generated method stub

				bitmap = Utils.getBitmapFromURL(url);
				return null;
			}

			@SuppressWarnings("deprecation")
			protected void onPostExecute(String result) {

				try {
					if (bitmap != null) {
						if (orientation == Configuration.ORIENTATION_LANDSCAPE)
							rlDepartment
									.setBackgroundDrawable(new BitmapDrawable(
											getResources(), bitmap));
						else if (!loadImageFlag)
							ivDepartBg.setImageBitmap(bitmap);
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				super.onPostExecute(result);
			}
		}.execute();

	}

	private void updateLCD(View v) {

		if (orientation == Configuration.ORIENTATION_PORTRAIT) {
			tvDepartDesc1 = (TextView) v.findViewById(R.id.tvDepartDesc1);
			tvDepartDesc1.setVisibility(View.GONE);
			if (PRJFUNC.mGrp == null) {
				PRJFUNC.resetGraphValue(getActivity());
			}

			if (horizontalListView != null) {
				rlDepartInfo.setVisibility(View.VISIBLE);
				// tvDepartDesc1.setVisibility(View.GONE);
				horizontalListView.setVisibility(View.GONE);

			}

			options = new DisplayImageOptions.Builder()
					.showImageOnLoading(R.drawable.bg_default_category)
					.showImageForEmptyUri(R.drawable.bg_default_category)
					.showImageOnFail(R.drawable.bg_default_category)
					.cacheInMemory(false).cacheOnDisc(true)
					.bitmapConfig(Bitmap.Config.RGB_565).build();

			ivDepartBg = (ImageView) v.findViewById(R.id.ivDepartBg);
			tvDepartDesc = (TextView) v.findViewById(R.id.tvDepartDesc);
			tvDepartDesc1 = (TextView) v.findViewById(R.id.tvDepartDesc1);
			m_adtCategoryList = new CategoryListAdapter(getActivity(),
					R.layout.item_category, new ArrayList<CategoryInfo>());

			// lvCategoryList.setSelector(new ColorDrawable(Color.TRANSPARENT));
			lvCategoryList.setCacheColorHint(Color.TRANSPARENT);
			lvCategoryList.setDividerHeight(0);
			lvCategoryList.setAdapter(m_adtCategoryList);
			lvCategoryList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {

					CategoryInfo categoryInfo = (CategoryInfo) m_adtCategoryList
							.getItem(position);
					if (categoryInfo.hasSubCat == 0)
						((MainActivity) getActivity()).goItemList(departInfo,
								categoryInfo);
					else
						((MainActivity) getActivity()).goSubCategory(
								departInfo, categoryInfo);
				}
			});

			pbCategory = (ProgressBar) v.findViewById(R.id.pbCategory);
			pbCategory.setVisibility(View.INVISIBLE);

			PRJFUNC.mGrp.relayoutView(v.findViewById(R.id.ivShadowTop),
					LayoutLib.LP_RelativeLayout);
		} else {

			if (PRJFUNC.mGrp == null) {
				PRJFUNC.resetGraphValue(getActivity());
			}

			options = new DisplayImageOptions.Builder()
					.showImageOnLoading(R.drawable.bg_default_category)
					.showImageForEmptyUri(R.drawable.bg_default_category)
					.showImageOnFail(R.drawable.bg_default_category)
					.cacheInMemory(false).cacheOnDisc(true)
					.bitmapConfig(Bitmap.Config.RGB_565).build();

			ivDepartBg = (ImageView) v.findViewById(R.id.ivDepartBg);
			tvDepartDesc = (TextView) v.findViewById(R.id.tvDepartDesc);
			tvDepartDesc1 = (TextView) v.findViewById(R.id.tvDepartDesc1);
			tvDepartDesc1.setText(departInfo.desc);
			m_adtCategoryList = new CategoryListAdapter(getActivity(),
					R.layout.land_item_category, new ArrayList<CategoryInfo>());

			// lvCategoryList.setSelector(new ColorDrawable(Color.TRANSPARENT));
			// horizontalListView.setCacheColorHint(Color.TRANSPARENT);
			// lvCategoryList.setDividerHeight(0);
			horizontalListView.setAdapter(m_adtCategoryList);
			if (lvCategoryList != null) {
				// rlDepartInfo.setVisibility(View.GONE);
				lvCategoryList.setVisibility(View.GONE);
				tvDepartDesc.setVisibility(View.GONE);
				tvDepartDesc1.setVisibility(View.VISIBLE);
				tvDepartDesc1.setText(departInfo.desc);
			}

			horizontalListView
					.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int position, long arg3) {

							CategoryInfo categoryInfo = (CategoryInfo) m_adtCategoryList
									.getItem(position);
							if (categoryInfo.hasSubCat == 0)
								((MainActivity) getActivity()).goItemList(
										departInfo, categoryInfo);
							else
								((MainActivity) getActivity()).goSubCategory(
										departInfo, categoryInfo);
						}
					});

			pbCategory = (ProgressBar) v.findViewById(R.id.pbCategory);
			pbCategory.setVisibility(View.INVISIBLE);

			PRJFUNC.mGrp.relayoutView(v.findViewById(R.id.ivShadowTop),
					LayoutLib.LP_RelativeLayout);
		}
	}

	private void scaleView(View v) {

		if (PRJFUNC.mGrp == null) {
			return;
		}

		PRJFUNC.mGrp.relayoutView(rlDepartInfo, LayoutLib.LP_RelativeLayout);
		PRJFUNC.mGrp.setTextViewFontScale(tvDepartDesc);
		PRJFUNC.mGrp.repaddingView(tvDepartDesc);

		PRJFUNC.mGrp.relayoutView(v.findViewById(R.id.ivShadowTop),
				LayoutLib.LP_RelativeLayout);
		PRJFUNC.mGrp.relayoutView(v.findViewById(R.id.ivShadowBottom),
				LayoutLib.LP_RelativeLayout);
	}

	@Override
	public void onDestroy() {

		CustomerHttpClient.stop();

		ImageLoader.getInstance().stop();
		ImageLoader.getInstance().clearMemoryCache();
		super.onDestroy();
	}

	public void setDepartInfo(DepartInfo departInfo) {
		this.departInfo = departInfo;
	}

	public void loadCategoryList() {

		m_adtCategoryList.clear();

		if (NetworkUtils.haveInternet(getActivity())) {

			// . Restaurant :
			// https://www.appyorder.com/pro_version/webservice_smart_app/new/GetMainCategory.php?hotel_id=6759
			// . Department :
			// https://www.appyorder.com/pro_version/webservice_smart_app/Department/GetMainCategory.php?hotel_id=6759&dept_id=1
			String hotel_id = PrefValue.getString(getActivity(),
					R.string.pref_hotel_id);
			String url = "new/GetMainCategory.php";
			RequestParams params = new RequestParams();
			params.add("hotel_id", hotel_id);

			if (departInfo.id > 0) {
				url = "Department/GetMainCategory.php";
				params.add("dept_id", String.valueOf(departInfo.id));
			}

			pbCategory.setVisibility(View.VISIBLE);
			CustomerHttpClient.get(url, params, new AsyncHttpResponseHandler() {
				@Override
				public void onFinish() {

					pbCategory.setVisibility(View.INVISIBLE);
					super.onFinish();
				}

				@Override
				public void onProgress(int bytesWritten, int totalSize) {

					pbCategory.setProgress(bytesWritten / totalSize * 100);
					super.onProgress(bytesWritten, totalSize);
				}

				@Override
				public void onFailure(int statusCode, Header[] headers,
						byte[] errorResponse, Throwable e) {

					Toast.makeText(getActivity(),
							"Connection was lost (" + statusCode + ")",
							Toast.LENGTH_LONG).show();
					super.onFailure(statusCode, headers, errorResponse, e);
				}

				@Override
				public void onSuccess(int statusCode, Header[] headers,
						byte[] response) {
					// Pull out the first event on the public timeline
					try {

						/*
						 * { "status": "true", "data": [ { "id": "986", "thumb":
						 * "http://roomallocator.com/appybiteRestaurant/category_thumbnail/small_455158room-service.jpg"
						 * , "title": "Roomservice", "has_Sub_Category": "1" } ]
						 * }
						 */

						String result = new String(response);
						result = result.replace("({", "{");
						result = result.replace("})", "}");
						Log.i("HTTP Response <<<", result);
						JSONObject jsonObject = new JSONObject(result);
						JSONArray data = jsonObject.getJSONArray("data");

						for (int i = 0; i < data.length(); i++) {

							CategoryInfo item = new CategoryInfo();

							JSONObject object = data.getJSONObject(i);
							item.id = object.getString("id");
							item.name = object.has("name") ? object
									.getString("name") : object
									.getString("title");
							item.thumb = object.getString("thumb");
							item.hasSubCat = object.getInt("has_Sub_Category");

							m_adtCategoryList.add(item);
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Toast.makeText(getActivity(), "Invalid Data",
								Toast.LENGTH_LONG).show();
					}
				}
			});

		} else {
			Toast.makeText(getActivity(), "No Internet Connection",
					Toast.LENGTH_LONG).show();
		}
	}

}
