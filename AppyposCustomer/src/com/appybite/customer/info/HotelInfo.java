package com.appybite.customer.info;

public class HotelInfo
{
	/*
    {
        "hotel_id": "5162",
        "hotel_name": "as",
        "no_rooms": "22",
        "country": "",
        "phone_no": "2e",
        "address": "Street Addresssadas",
        "hotel_desc": "2323",
        "website": "23sad",
        "lat": "0",
        "long": "0"
    }
    */
	public int id;
	public int hotel_id;
	public String hotel_name;
	public String hotel_logo;
	public String no_rooms;
	public String country;
	public String phone_no;
	public String address;
	public String hotel_desc;
	public String website;
	public String lat;
	public String lon;
	
	public String license;
	public String hotel_bg;
}