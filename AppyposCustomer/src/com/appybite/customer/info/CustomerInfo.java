package com.appybite.customer.info;

public class CustomerInfo
{
	public String id;
	public String name;
	public String address;
	public String state;
	public String city;
	public String email_id;
	public String phone; //. true : available, false : busy
	public String p_code;
	public String country;
	
	public String order_type;
	public String no;
}