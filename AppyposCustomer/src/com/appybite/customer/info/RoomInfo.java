package com.appybite.customer.info;

public class RoomInfo
{
	/*
	 [
	    {
	        "room_id": "1",
	        "room_no": "101",
	        "room_name": "royale",
	        "no_of_beds": "2",
	        "type": "Apartment",
	        "price": "2"
	    }
	 ]
	 */
	public int room_id;
	public String room_no;
	public String room_name;
	public String no_of_beds;
	public String type; 
	public String price;
}