package com.appybite.customer.info;

public class ModifierInfo
{
	/*
	{
	    "id": "487",
	    "price": "47",
	    "name": "Package of botatos",
	    "property": "",
	    "group": "0",
	    "qnt": "0"
	} 
	 */

	public String id;
	public String title;
	public String price;
	public int qnt;
}