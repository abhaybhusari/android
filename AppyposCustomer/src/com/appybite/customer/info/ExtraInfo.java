package com.appybite.customer.info;

public class ExtraInfo
{
	/*
    {
        "id": "50",
        "price": "30",
        "title": "cheese",
        "image": "http://www.roomallocator.com/restaurant/extra_thumbnail/small_544686screenshot368jpg",
        "thumbnail": "http://www.roomallocator.com/restaurant/extra_thumbnail/small_544686screenshot368jpg",
        "qnt": "0"
    }
	 */

	public String id;
	public String title;
	public String price;
	public String image;
	public String thumb;
	public int qnt;
}