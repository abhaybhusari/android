package com.appybite.customer.info;

public class ItemInfo
{
	/*
	{
	    "id": "1796",
	    "title": "Cajun Sizzler",
	    "desc": "Sizzling Cajun chicken, pepperoni, Roquito ? chilli peppers, mixed peppers and red onions.",
	    "price": "23",
	    "image": "http://www.appyorder.com/appybiteRestaurant/predefineItems/item_thumbnail/img_976563",
	    "thumb": "http://www.appyorder.com/appybiteRestaurant/predefineItems/item_thumbnail/small_976563"
	} 
	/*{"id":"271",
	 * "name":"a",
	 * "image":"http:\/\/www.appypos.com\/appcreator\/uploads\/Screenshot from 2014-02-18 23:08:19.png",
	 * "dep_id":"1",
	 * "dept_name":"House-Keeping",
	 * "price":"44",
	 * "description":"gggggggggggg"}
	 */

	public String id;
	public String title;
	public String desc;
	public String price;
	public String image;
	public String thumb;
	public String video;
	public int qnt;
	public String msg;
	public double rate;
	
	public String dep_id;
	public String dept_name;
}