package com.appybite.customer.info;

public class DepartInfo
{
	public int id;
	public String title;
	public String desc;
	public String image;
	public boolean isRestaurant;
}