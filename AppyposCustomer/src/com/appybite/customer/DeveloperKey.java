// Copyright 2012 Google Inc. All Rights Reserved.

package com.appybite.customer;

/**
 * Static container class for holding a reference to your YouTube Developer Key.
 */
public class DeveloperKey {

  /**
   * Please replace this with a valid API key which is enabled for the 
   * YouTube Data API v3 service. Go to the 
   * <a href="https://code.google.com/apis/console/">Google APIs Console</a> to
   * register a new developer key.
   */
  public static final String DEVELOPER_KEY = "AI39si4xOP82N43DAc2yieCOYsG6kxImUgrZUG2ey9tL6MsA06Y3rYCIL4Nk9JsUVall8xr4L4Q8Z3ujQV8lek51B6q5SMBJmw";

}
